package main

import (
	"strings"
	"testing"
)

func TestFullPath(t *testing.T) {
	f := "/fullpath/to/folder"
	s := PathToFullPath(f)
	if f != s {
		t.Error("Send in string that starts with \"/\" should return same string back, " + f + " is not equal to " + s)
	}
}

func TestRelativePath(t *testing.T) {
	f := "folder"
	s := PathToFullPath(f)
	if !strings.HasPrefix(s, "/") {
		t.Error("Send in string that not starts with \"/\" should return a string that starts with /, " + f + " is not equal to " + s)
	}
}
